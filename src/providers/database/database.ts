import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import 'rxjs/add/observable/from';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  private database : any;
  private data: any;
  private success : boolean = true;
  remote: string = 'http://127.0.0.1:8091/ui/index.html#!/buckets/documents?bucket=homecms';

  constructor() {

    this.database = new PouchDB('homecms');

    /*this.database.destroy(function (err, response) {

      if (err) {
         return console.log(err);
      } else {
         console.log ('Database Deleted');
      }

    });*/

    let options = {
      live: true,
      retry: true
    };
    
    this.database.info().then(function (info) {
      //console.info(info);
    });

    this.database.sync(this.remote, options);

  }

  handleChange(change){
    
     let changedDoc = null;
     let changedIndex = null;
    
     this.data.forEach((doc, index) => {
    
       if(doc._id === change.id){
         changedDoc = doc;
         changedIndex = index;
       }
    
     });
    
     //A document was deleted
     if(change.deleted){
       this.data.splice(changedIndex, 1);
     }
     else {
    
       //A document was updated
       if(changedDoc){
         this.data[changedIndex] = change.doc;
       }
    
       //A document was added
       else {
         this.data.push(change.doc);
       }
    
     }
    
   }

  retrieveSystemAreas() : any {

    if (this.data) {
      return Promise.resolve(this.data);
    }
   
    return new Promise(resolve => {
   
      this.database.allDocs({
   
        include_docs: true
   
      })
      .then((result) => {
   
        this.data = [];
   
        let docs = result.rows.map((row) => {
          this.data.push(row.doc);
        });

        resolve(this.data);
   
        this.database.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
          this.handleChange(change);
        });
   
      }).catch((error) => {
   
        console.log(error);
   
      });
   
    });

  }

  saveSystemArea(systemArea) {

    var timeStamp  = new Date().toISOString(),
        systemAreas = { _id : timeStamp,
                        documentType: "systemArea",
                        title : systemArea.title,
                        description : systemArea.description};

    return new Promise(resolve => {


      if (systemArea.title != null) {

         this.database.put(systemAreas).catch((err) => {
            this.success = false;
         });

         resolve(true);

      }

    });

  }

  deleteSystemArea (_id, _rev) {

    this.database.remove(_id, _rev, function(err) {
      if (err) {
         return console.log(err);
      } else {
         console.log("Document deleted successfully");
      }
    });

  }

}
