import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

export interface interfaceSystemArea { 
  title: string; 
}

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})

export class ListPage {

  currentSystemArea: string;
  systemAreas : any;

  getSystemAreas () {
    
    this.systemAreas = this.database.retrieveSystemAreas();

  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public database : DatabaseProvider) {

    this.getSystemAreas();

  } 

  addSystemArea() {

    var newSystemArea = {title : this.currentSystemArea,
                         description : this.currentSystemArea};

      this.database.saveSystemArea(newSystemArea);
      this.currentSystemArea = null;

  }

  removeSystemArea(_id, _rev) {

    this.database.deleteSystemArea(_id, _rev);

  }

  loadSystemAreaItems(systemArea) {

     this.navCtrl.push('ListDetailPage', { nameSytemArea : systemArea});

  }

}
