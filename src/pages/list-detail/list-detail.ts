import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

/**
 * Generated class for the ListDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-detail',
  templateUrl: 'list-detail.html',
})
export class ListDetailPage {

  nameSytemArea: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
     this.nameSytemArea = this.navParams.get('nameSytemArea');
  }

  ionViewDidLoad (database : DatabaseProvider) {

    console.log('ionViewDidLoad ListDetailPage');
  }

}
